package com.imooc.demo.config.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.TransactionManagementConfigurer;

import javax.sql.DataSource;

/**
 * @ClassName TransactionManagementConfigurantion
 * @Description TODO
 * @Author fulh
 * @Date 2018/12/10 16:20
 * @Version 1.0
 */
@Configuration
@EnableTransactionManagement
public class TransactionManagementConfigurantion implements TransactionManagementConfigurer{

    @Autowired
    @Qualifier("dataSource")
    private DataSource dataSource;

    @Override
    public PlatformTransactionManager annotationDrivenTransactionManager() {
        return new DataSourceTransactionManager(dataSource);
    }
}

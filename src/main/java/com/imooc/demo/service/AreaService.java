package com.imooc.demo.service;

import com.imooc.demo.entity.Area;

import java.util.List;

/**
 * @ClassName AreaService
 * @Description TODO
 * @Author fulh
 * @Date 2018/12/10 16:23
 * @Version 1.0
 */
public interface AreaService {
    /**
     *@Description  列出区域列表
     *@Author fulh
     *@Date 2018/12/10 8:54
     *@Param
     *@return
     */
    List<Area> getAreaList();

    /**
     *@Description  根据ID列出具体区域
     *@Author fulh
     *@Date 2018/12/10 8:53
     *@Param
     *@return
     */
    Area getAreaById(int areaId);

    /**
     *@Description  插入区域信息
     *@Author fulh
     *@Date 2018/12/10 8:53
     *@Param
     *@return
     */
    boolean addArea(Area area);

    /**
     *@Description  更新区域信息
     *@Author fulh
     *@Date 2018/12/10 8:52
     *@Param
     *@return
     */
    boolean modifyArea(Area area);

    /**
     *@Description  删除区域信息
     *@Author fulh
     *@Date 2018/12/10 8:52
     *@Param
     *@return
     */
    boolean deleteArea(int areaId);
}

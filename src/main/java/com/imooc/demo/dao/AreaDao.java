package com.imooc.demo.dao;

import com.imooc.demo.entity.Area;

import java.util.List;

/**
 * @ClassName AreaDao
 * @Description TODO
 * @Author fulh
 * @Date 2018/12/10 8:50
 * @Version 1.0
 */
public interface AreaDao {

    /**
     *@Description  列出区域列表
     *@Author fulh
     *@Date 2018/12/10 8:54
     *@Param
     *@return
     */
    List<Area> queryArea();

    /**
     *@Description  根据ID列出具体区域
     *@Author fulh
     *@Date 2018/12/10 8:53
     *@Param
     *@return
     */
    Area queryAreaById(int areaId);

    /**
     *@Description  插入区域信息
     *@Author fulh
     *@Date 2018/12/10 8:53
     *@Param
     *@return
     */
    int insertArea(Area area);

    /**
     *@Description  更新区域信息
     *@Author fulh
     *@Date 2018/12/10 8:52
     *@Param
     *@return
     */
    int updateArea(Area area);

    /**
     *@Description  删除区域信息
     *@Author fulh
     *@Date 2018/12/10 8:52
     *@Param
     *@return
     */
    int deleteArea(int areaId);
}
